#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "dieNdice.h"

using namespace std;

TEST_CASE("Test die roll functionality") {

	die aDie1;
	aDie1.roll();

	SECTION("Die #1 roll test") {
		REQUIRE(aDie1.getVal() > 0);
		REQUIRE(aDie1.getVal() < 7);
	}

	die aDie2;
	aDie2.roll();

	SECTION("Die #2 roll test") {
		REQUIRE(aDie2.getVal() > 0);
		REQUIRE(aDie2.getVal() < 7);
	}

	die aDie3;
	aDie3.roll();

	SECTION("Die #3 roll test") {
		REQUIRE(aDie3.getVal() > 0);
		REQUIRE(aDie3.getVal() < 7);
	}

	die aDie4;
	aDie4.roll();

	SECTION("Die #4 roll test") {
		REQUIRE(aDie4.getVal() > 0);
		REQUIRE(aDie4.getVal() < 7);
	}

	die aDie5;
	aDie5.roll();

	SECTION("Die #5 roll test") {
		REQUIRE(aDie5.getVal() > 0);
		REQUIRE(aDie5.getVal() < 7);
	}

}