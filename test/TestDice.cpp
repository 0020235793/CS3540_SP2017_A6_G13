#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "dieNdice.h"

//Update
using namespace std;

TEST_CASE("Test die roll functionality") {

	die aDie1, aDie2, aDie3, aDie4, aDie5;
	dice allDie;

	allDie.rollDice(aDie1, aDie2, aDie3, aDie4, aDie5);

	SECTION("Dice Roll Test") {
		REQUIRE(aDie1.getVal() > 0);
		REQUIRE(aDie1.getVal() < 7);
		REQUIRE(aDie2.getVal() > 0);
		REQUIRE(aDie2.getVal() < 7);
		REQUIRE(aDie3.getVal() > 0);
		REQUIRE(aDie3.getVal() < 7);
		REQUIRE(aDie4.getVal() > 0);
		REQUIRE(aDie4.getVal() < 7);
		REQUIRE(aDie5.getVal() > 0);
		REQUIRE(aDie5.getVal() < 7);

	}

}