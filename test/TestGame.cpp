#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "../include/Game.h"
#include "../include/dieNdice.h"
#include "../include/ScoreCard.h"

using namespace std;

TEST_CASE("Test core functionality", "Game") {


	Game g;
	int  a[0];
	int b[1] = {-1};
	int c[13] = {0,1,2,3,4,5,6,7,8,9,10,11,12};

	SECTION("Testing Game Class") {
		REQUIRE(g.Restart("y") == true);
		REQUIRE(g.Restart("n") == false);
		REQUIRE(g.CheckEntry(a, -1) == false);
		REQUIRE(g.CheckEntry(b, -1) == true);
		REQUIRE(g.CheckEntry(c, 12) == true);
		
	}
}