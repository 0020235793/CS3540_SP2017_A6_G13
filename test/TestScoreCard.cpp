#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "Game.h"
#include "dieNdice.h"
#include "ScoreCard.h"

using namespace std;

TEST_CASE("Test scorecard class"){

	scoreCard score;
	int D[5];
	D[0] = 1;
	D[1] = 2;
	D[2] = 3;
	D[3] = 4;
	D[4] = 5;

		SECTION("Testing ScoreCard Calculation Functions 0-5") {
		REQUIRE(score.Calc0(D) == 1);
		REQUIRE(score.Calc1(D) == 2);
		REQUIRE(score.Calc2(D) == 3);
		REQUIRE(score.Calc3(D) == 4);
		REQUIRE(score.Calc4(D) == 5);
		REQUIRE(score.Calc5(D) == 0);
	}

	SECTION("Testing Three of a kind"){

	for(int i = 1; i < 7; i++){
		D[2] = i;
		D[3] = i;
		D[4] = i;
	
		REQUIRE(score.calc3ofKind(D) == (D[0] + D[1] + D[2] + D[3] + D[4]));
	}

	for(int i = 1; i < 7; i++){
		D[0] = i;
		D[1] = i;
		D[2] = i;
	
		REQUIRE(score.calc3ofKind(D) == (D[0] + D[1] + D[2] + D[3] + D[4]));
	}

	D[0] = 1;
	D[1] = 2;
	D[2] = 3;
	D[3] = 4;
	D[4] = 5;

	REQUIRE(score.calc3ofKind(D) == 0);
	}

	SECTION("Testing Four of a kind"){
	
	for(int i = 1; i < 7; i++){
		D[1] = i;
		D[2] = i;
		D[3] = i;
		D[4] = i;
	
		REQUIRE(score.calc4ofKind(D) == (D[0] + D[1] + D[2] + D[3] + D[4]));
	}

	for(int i = 1; i < 7; i++){
		D[0] = i;
		D[1] = i;
		D[2] = i;
		D[3] = i;
		D[4] = 5;
	
		REQUIRE(score.calc4ofKind(D) == (D[0] + D[1] + D[2] + D[3] + D[4]));
	}

	D[0] = 1;
	D[1] = 2;
	D[2] = 3;
	D[3] = 4;
	D[4] = 5;

	REQUIRE(score.calc4ofKind(D) == 0);

	for(int i = 1; i < 7; i++){
		D[0] = i;
		D[1] = i;
		D[3] = i;
		D[4] = i;
	
		REQUIRE(score.calc4ofKind(D) == (D[0] + D[1] + D[2] + D[3] + D[4]));
	}

	}

	SECTION("Testing Full House"){
	for(int i = 1; i < 5; i++){
		D[0] = i + 1;
		D[1] = i + 1;
		D[2] = i;
		D[3] = i;
		D[4] = i;
	
		REQUIRE(score.calcFullHouse(D) == 25);
	}

	D[0] = 1;
	D[1] = 2;
	D[2] = 3;
	D[3] = 4;
	D[4] = 5;

	REQUIRE(score.calcFullHouse(D) == 0);
	}

	SECTION("Testing Small Straight"){
	for(int i = 1; i < 4; i++){
		D[0] = i;
		D[1] = i+1;
		D[2] = i+2;
		D[3] = i+3;
		D[4] = i+4;
	
		REQUIRE(score.calcSmStraight(D) == 30);
	}

	D[0] = 1;
	D[1] = 2;
	D[2] = 1;
	D[3] = 2;
	D[4] = 1;

	REQUIRE(score.calcSmStraight(D) == 0);

	D[0] = 5;
	D[1] = 2;
	D[2] = 4;
	D[3] = 4;
	D[4] = 3;

	REQUIRE(score.calcSmStraight(D) == 30);
	}

	SECTION("Testing Large Straight"){
	
	for(int i = 1; i < 3; i++){
		D[0] = i;
		D[1] = i+1;
		D[2] = i+2;
		D[3] = i+3;
		D[4] = i+4;
	
		REQUIRE(score.calcLgStraight(D) == 40);
	}

	D[0] = 1;
	D[1] = 2;
	D[2] = 1;
	D[3] = 2;
	D[4] = 1;

	REQUIRE(score.calcLgStraight(D) == 0);
	

	D[0] = 3;
	D[1] = 2;
	D[2] = 4;
	D[3] = 5;
	D[4] = 1;

	REQUIRE(score.calcLgStraight(D) == 40);
	}

	SECTION("Testing Yahtzee"){
	
	for(int i = 1; i < 7; i++){
		D[0] = i;
		D[1] = i;
		D[2] = i;
		D[3] = i;
		D[4] = i;
	
		REQUIRE(score.calcYahtzee(D) == 50);
	}

	D[0] = 1;
	D[1] = 2;
	D[2] = 1;
	D[3] = 2;
	D[4] = 1;

	REQUIRE(score.calcYahtzee(D) == 0);
	}

	SECTION("Testing Chance"){
	
	for(int i = 1; i < 7; i++){
		D[0] = i;
		D[1] = i;
		D[2] = i;
		D[3] = i;
		D[4] = i;
	
		REQUIRE(score.calcChance(D) == (D[0] + D[1] + D[2] + D[3] + D[4]));
		}
	}

	SECTION("Testing Display Function"){
		D[0] = 4;
		D[1] = 3;
		D[2] = 6;
		D[3] = 2;
		D[4] = 5;

		score.testDisplay(D);

		REQUIRE(score.getDisp(6) == score.calc3ofKind(D));
		REQUIRE(score.getDisp(7) == score.calc4ofKind(D));
		REQUIRE(score.getDisp(8) == score.calcFullHouse(D));
		REQUIRE(score.getDisp(9) == score.calcSmStraight(D));
		REQUIRE(score.getDisp(10) == score.calcLgStraight(D));
		REQUIRE(score.getDisp(11) == score.calcYahtzee(D));
		REQUIRE(score.getDisp(12) == score.calcChance(D));
	}

	SECTION("Testing Set Open Function"){
		score.setOpen();
		for(int i = 0; i < 14; i++){
			REQUIRE(score.getOpen(i) == true);
		}
	}

	SECTION("Testing Save Score Function"){
		REQUIRE(score.getOpen(12) == true);
		REQUIRE(score.calcTotal() == 0);
		score.saveScore(12);
		REQUIRE(score.getCard(12) == score.getDisp(12));
		REQUIRE(score.getOpen(12) == false);
		REQUIRE(score.calcTotal() == score.getCard(12));
	}

}