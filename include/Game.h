#include <iostream>

using namespace std;

class Game {
	private:
	bool replay;
	public:
		Game(){ replay = true;}
		void Play();
		bool Restart(string);
		bool CheckEntry(int [], int);
};