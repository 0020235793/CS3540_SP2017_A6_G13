#ifndef SCORECARD_H
#define SCORECARD_H
#include<iostream>
#include<iomanip>

using namespace std;

class scoreCard{

private:
	int Disp[14];
	int D[5];
	int Card[14];
	bool Open[14];
	int SaveVal;

public:
	int Calc0(int D[]);
	int Calc1(int D[]);
	int Calc2(int D[]);
	int Calc3(int D[]);
	int Calc4(int D[]);
	int Calc5(int D[]);
	int calc3ofKind(int dice[]);
	int calc4ofKind(int dice[]);
	int calcFullHouse(int dice[]);
	int calcSmStraight(int dice[]);
	int calcLgStraight(int dice[]);
	int calcYahtzee(int dice[]);
	int calcChance(int dice[]);
	int calcTotal();
	void display(int dice[]);
	void saveScore(int SaveVal);
	void setOpen();
	void setArrays();
	void Total();

	//For test use only
	int getDisp(int i);
	int getCard(int i);
	void testDisplay(int dice[]);
	bool getOpen(int i);
};


#endif
