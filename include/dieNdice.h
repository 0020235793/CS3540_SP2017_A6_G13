// File: prog2.cpp
// Description: The purpose of this program file is to define a class of each polygon or polygon type, with the correct level of inheritance, and to use polymorphism.
// Author: Michael Irick
// Course: CS 3350, Spring 2016
#ifndef _DIENDICE         
#define _DIENDICE

#include <iostream>
#include <string>
#include <math.h>

class die {
protected:
	int faceValue;
public:
	void roll();
	int getVal();
};
class dice : public die {
public:
	void rollDice(die &die1, die &die2, die &die3, die &die4, die &die5);
};
#endif