#include "dieNdice.h"
#include "math.h"
#include <iomanip>
#include <iostream>
#include <stdlib.h>

using namespace std;

void die::roll()
{
	faceValue = rand() % 6 + 1;
}
int die::getVal()
{
	return faceValue;
}
void dice::rollDice(die &die1, die &die2, die &die3, die &die4, die &die5)
{
	die1.roll();
	die2.roll();
	die3.roll();
	die4.roll();
	die5.roll();
}