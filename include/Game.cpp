#ifndef _GAME_H__
#define __GAME_H__

#include "Game.h"
#include "ScoreCard.h"
#include "dieNdice.h"

#include <iomanip>
#include <iostream>
#include <stdlib.h>

using namespace std;

bool Game :: Restart(string s)
{
	string input = "";

	cout << "Play Again? (y|n)";
	cin >> input;
	if(input == "y")
	{
		replay = true;
	}
	else if(input == "n"){
		replay = false;
	}
	return replay;
}

void Game :: Play()
{
	ScoreCard score;

	score.setOpen();
	score.setArrays();

	for(int i = 0; i < 13; i++)
	{
	

		die die1, die2, die3, die4, die5;

		dice allDie;

		allDie.rollDice(die1, die2, die3, die4, die5);

		int a [5] = {0,0,0,0,0};
		a[0] = die1.getVal(die1);
		a[1] = die2.getVal(die2);
		a[2] = die3.getVal(die3);
		a[3] = die4.getVal(die4);
		a[4] = die5.getVal(die5);

		score.display(a[0],a[1],a[2],a[3],a[4]);

	}
	score.Total();
}

bool Game :: CheckEntry(int track [], int a)
{
	bool found = false;
	for(int i = 0; i < 13; i++)
	{
		if(track[i] == a)
		{
			cout << "Invalid Entry" << endl;
			found = false;
		}
	}
	return found;
}

#endif