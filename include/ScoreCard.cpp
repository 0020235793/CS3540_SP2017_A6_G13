#include "ScoreCard.h"
#include <iostream>
#include <iomanip>

using namespace std;

	int scoreCard::Calc0(int D[])
	{
		int count = 0, score = 0;
		do
		{
			if (D[count] == 1)
				score++;
			count++;
		} while (count < 5);

		return (1 * score);
	}

	int scoreCard::Calc1(int D[])
	{
		int count = 0, score = 0;
		do
		{
			if (D[count] == 2)
				score++;
			count++;
		} while (count < 5);

		return (2 * score);
	}

	int scoreCard::Calc2(int D[])
	{
		int count = 0, score = 0;
		do
		{
			if (D[count] == 3)
				score++;
			count++;
		} while (count < 5);

		return (3 * score);
	}

	int scoreCard::Calc3(int D[])
	{
		int count = 0, score = 0;
		do
		{
			if (D[count] == 4)
				score++;
			count++;
		} while (count < 5);

		return (4 * score);
	}

	int scoreCard::Calc4(int D[])
	{
		int count = 0, score = 0;
		do
		{
			if (D[count] == 5)
				score++;
			count++;
		} while (count < 5);

		return (5 * score);
	}

	int scoreCard::Calc5(int D[])
	{
		int count = 0, score = 0;
		do
		{
			if (D[count] == 6)
				score++;
			count++;
		} while (count < 5);

		return (6 * score);
	}

int scoreCard::calc3ofKind(int dice[]){
	int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

	for (int i = 0; i < 5; i++)
	{
		if (dice[i] == 1)
			cnt1++;
		else if (dice[i] == 2)
			cnt2++;
		else if (dice[i] == 3)
			cnt3++;
		else if (dice[i] == 4)
			cnt4++;
		else if (dice[i] == 5)
			cnt5++;
		else if (dice[i] == 6)
			cnt6++;
	}

	if (cnt1 >= 3 || cnt2 >= 3 || cnt3 >= 3 || cnt4 >= 3 || cnt5 >= 3 || cnt6 >= 3)
		score = dice[0] + dice[1] + dice[2] + dice[3] + dice[4];

	return score;
}

int scoreCard::calc4ofKind(int dice[]){
	int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

	for (int i = 0; i < 5; i++)
	{
		if (dice[i] == 1)
			cnt1++;
		else if (dice[i] == 2)
			cnt2++;
		else if (dice[i] == 3)
			cnt3++;
		else if (dice[i] == 4)
			cnt4++;
		else if (dice[i] == 5)
			cnt5++;
		else if (dice[i] == 6)
			cnt6++;
	}

	if (cnt1 >= 4 || cnt2 >= 4 || cnt3 >= 4 || cnt4 >= 4 || cnt5 >= 4 || cnt6 >= 4)
		score = dice[0] + dice[1] + dice[2] + dice[3] + dice[4];

	return score;
}

int scoreCard::calcFullHouse(int dice[]){
	int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

	for (int i = 0; i < 5; i++)
	{
		if (dice[i] == 1)
			cnt1++;
		else if (dice[i] == 2)
			cnt2++;
		else if (dice[i] == 3)
			cnt3++;
		else if (dice[i] == 4)
			cnt4++;
		else if (dice[i] == 5)
			cnt5++;
		else if (dice[i] == 6)
			cnt6++;
	}
	
	if((cnt1 == 3 || cnt2 == 3 || cnt3 == 3 || cnt4 == 3 || cnt5 == 3 || cnt6 == 3) && (cnt1 == 2 || cnt2 == 2 || cnt3 == 2 || cnt4 == 2 || cnt5 == 2 || cnt6 == 2))
		score = 25;

	return score;

}

int scoreCard::calcSmStraight(int dice[]){
	int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

	for (int i = 0; i < 5; i++)
	{
		if (dice[i] == 1)
			cnt1++;
		else if (dice[i] == 2)
			cnt2++;
		else if (dice[i] == 3)
			cnt3++;
		else if (dice[i] == 4)
			cnt4++;
		else if (dice[i] == 5)
			cnt5++;
		else if (dice[i] == 6)
			cnt6++;
	}

	if (cnt1 >= 1 && cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1)
		score = 30;
	else if (cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1)
		score = 30;
	else if (cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1 && cnt6 >= 1)
		score = 30;

	return score;

}

int scoreCard::calcLgStraight(int dice[]){
	int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

	for (int i = 0; i < 5; i++)
	{
		if (dice[i] == 1)
			cnt1++;
		else if (dice[i] == 2)
			cnt2++;
		else if (dice[i] == 3)
			cnt3++;
		else if (dice[i] == 4)
			cnt4++;
		else if (dice[i] == 5)
			cnt5++;
		else if (dice[i] == 6)
			cnt6++;
	}

	if (cnt1 >= 1 && cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1)
		score = 40;
	else if (cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1 && cnt6 >= 1)
		score = 40;

	return score;
}

int scoreCard::calcYahtzee(int dice[]){
	if(dice[0] == dice[1] && dice[1] == dice[2] && dice[2] == dice[3] && dice[3] == dice[4])
		return 50;
	else
		return 0;
}

int scoreCard::calcChance(int dice[]){
	return (dice[0] + dice[1] + dice[2] + dice[3] + dice[4]);
}

void scoreCard::display(int dice[]){
	Disp[6] = calc3ofKind(dice);
	Disp[7] = calc4ofKind(dice);
	Disp[8] = calcFullHouse(dice);
	Disp[9] = calcSmStraight(dice);
	Disp[10] = calcLgStraight(dice);
	Disp[11] = calcYahtzee(dice);
	Disp[12] = calcChance(dice);


	if(Open[0])
		cout << setw(20) << "1.Ones: " << "|" << setw(4) << Disp[6] << "|" << endl;
	if(Open[1])
		cout << setw(20) << "2.Twos: " << "|" << setw(4) << Disp[7] << "|" << endl;
	if(Open[2])
		cout << setw(20) << "3.Threes: " << "|" << setw(4) << Disp[8] << "|" << endl;
	if(Open[3])
		cout << setw(20) << "4.Fours: " << "|" << setw(4) << Disp[9] << "|" << endl;
	if(Open[4])
		cout << setw(20) << "5.Fives: " << "|" << setw(4) << Disp[10] << "|" << endl;
	if(Open[5])
		cout << setw(20) << "6.Sixes: " << "|" << setw(4) << Disp[11] << "|" << endl;
	if(Open[6])
		cout << setw(20) << "7.Three of a kind: " << "|" << setw(4) << Disp[6] << "|" << endl;
	if(Open[7])
		cout << setw(20) << "8.Four of a kind: " << "|" << setw(4) << Disp[7] << "|" << endl;
	if(Open[8])
		cout << setw(20) << "9.Full House: " << "|" << setw(4) << Disp[8] << "|" << endl;
	if(Open[9])
		cout << setw(20) << "10.Small Straight: " << "|" << setw(4) << Disp[9] << "|" << endl;
	if(Open[10])
		cout << setw(20) << "11.Large Straight: " << "|" << setw(4) << Disp[10] << "|" << endl;
	if(Open[11])
		cout << setw(20) << "12.Yahtzee: " << "|" << setw(4) << Disp[11] << "|" << endl;
	if(Open[12])
		cout << setw(20) << "13.Chance: " << "|" << setw(4) << Disp[12] << "|" << endl;

	cout << endl;
	cout << "What score would you like to save: ";
	cin >> SaveVal;

	while((SaveVal < 1) || (SaveVal > 13) || (Open[SaveVal] == false)){
		cout << "Selected score not available." << endl;
		cout << "Please enter a valid score you would like to save: ";
		cin >> SaveVal;
	}

	SaveVal = SaveVal - 1;

	saveScore(SaveVal);

	Card[13] = calcTotal();
}

int scoreCard::getDisp(int i){
	return Disp[i];
}

int scoreCard::getCard(int i){
	return Card[i];
}

void scoreCard::saveScore(int SaveVal){
	Card[SaveVal] = Disp[SaveVal];
	Open[SaveVal] = false;
}

void scoreCard::testDisplay(int dice[]){
	Disp[6] = calc3ofKind(dice);
	Disp[7] = calc4ofKind(dice);
	Disp[8] = calcFullHouse(dice);
	Disp[9] = calcSmStraight(dice);
	Disp[10] = calcLgStraight(dice);
	Disp[11] = calcYahtzee(dice);
	Disp[12] = calcChance(dice);
}

void scoreCard::setOpen(){
	for(int i = 0; i < 14; i++){
		Open[i] = true;
	}
}

void scoreCard::setArrays(){
	for(int i = 0; i < 14; i ++){
		Card[i] = 0;
		Disp[i] = 0;
	}
}

bool scoreCard::getOpen(int i){
	return Open[i];
}

int scoreCard::calcTotal(){
	int total = 0;

	for (int i = 0; i < 13; i++)
		total += Card[i];

	return total;
}

void scoreCard::Total(){
	cout << "Final Score" << endl;
	for(int i = 0; i < 13; i++){
		cout << Card[i] << endl;
	}
	cout << "Total Score" << endl << Card[13] << endl;
}